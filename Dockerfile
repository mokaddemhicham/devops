FROM openjdk:17-jdk-slim
RUN apt-get update && apt-get install maven -y
WORKDIR /app
COPY src ./src
COPY pom.xml .
RUN mvn clean install -DskipTests
EXPOSE 8080
CMD ["java", "-jar", "target/devops-0.0.1-SNAPSHOT.jar"]