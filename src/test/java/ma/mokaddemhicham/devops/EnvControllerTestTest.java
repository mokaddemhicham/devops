package ma.mokaddemhicham.devops;

import ma.mokaddemhicham.devops.controllers.EnvController;
import ma.mokaddemhicham.devops.services.EnvService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EnvController.class)
@ActiveProfiles("test")
class EnvControllerTestTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EnvService envService;

    @Test
    void getEnv_ShouldReturnTestProfile() throws Exception {
        // Arrange
        when(envService.getEnv()).thenReturn("test");

        // Act & Assert
        mockMvc.perform(get("/env"))
                .andExpect(status().isOk())
                .andExpect(content().string("test"));
    }

}
