-- SQL script to initialize the database with 10 students
INSERT INTO student (first_name, last_name, email, cin, field, average) VALUES
        ('Hassan', 'El Fad', 'elfad.ensa@uhp.ac.ma', 'A123456', 'GI S6', 14.5),
        ('Mohammed', 'Bakkali', 'bakkali.ensa@uhp.ac.ma', 'B234567', 'GI S8', 15.2),
        ('Fatima', 'Zahra', 'zahra.ensa@uhp.ac.ma', 'C345678', 'ISIBD S6', 13.8),
        ('Khadija', 'Bennis', 'bennis.ensa@uhp.ac.ma', 'D456789', 'ISIBD S8', 16.0),
        ('Youssef', 'Amrani', 'amrani.ensa@uhp.ac.ma', 'E567890', 'GI S6', 12.4),
        ('Ahmed', 'Tazi', 'tazi.ensa@uhp.ac.ma', 'F678901', 'GI S8', 14.7),
        ('Saida', 'Berrada', 'berrada.ensa@uhp.ac.ma', 'G789012', 'ISIBD S6', 15.6),
        ('Omar', 'Naciri', 'naciri.ensa@uhp.ac.ma', 'H890123', 'ISIBD S8', 13.9),
        ('Imane', 'Chraibi', 'chraibi.ensa@uhp.ac.ma', 'I901234', 'GI S6', 16.2),
        ('Ali', 'Fassi', 'fassi.ensa@uhp.ac.ma', 'J012345', 'GI S8', 14.3);
