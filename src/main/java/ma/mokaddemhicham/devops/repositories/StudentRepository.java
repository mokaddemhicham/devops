package ma.mokaddemhicham.devops.repositories;

import ma.mokaddemhicham.devops.entities.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    public Student findByCin(String cin);
}
