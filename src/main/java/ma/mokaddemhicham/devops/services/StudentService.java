package ma.mokaddemhicham.devops.services;

import lombok.RequiredArgsConstructor;
import ma.mokaddemhicham.devops.entities.Student;
import ma.mokaddemhicham.devops.repositories.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;

    public Student saveStudent(Student student) {
        return studentRepository.save(student);
    }

    public Student getStudent(Long id) {
        return studentRepository.findById(id).orElse(null);
    }

    public Student getStudentByCin(String cin) {
        return studentRepository.findByCin(cin);
    }

    public List<Student> getStudents() {
        return studentRepository.findAll();
    }

    public void deleteStudent(Long id) {
        studentRepository.deleteById(id);
    }
}
