    package ma.mokaddemhicham.devops.services;

    import lombok.RequiredArgsConstructor;
    import org.springframework.core.env.Environment;
    import org.springframework.stereotype.Service;

    @Service
    @RequiredArgsConstructor
    public class EnvService {

        private final Environment environment;

        public String getEnv() {
            return environment.getActiveProfiles()[0];
        }
    }
