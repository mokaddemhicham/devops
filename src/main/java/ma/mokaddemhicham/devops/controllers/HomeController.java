package ma.mokaddemhicham.devops.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HomeController {

    @RequestMapping("")
    public String home() {
        return "Welcome to DevOps Application V2.0, this is the home page.";
    }
}
