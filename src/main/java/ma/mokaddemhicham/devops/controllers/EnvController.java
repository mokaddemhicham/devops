package ma.mokaddemhicham.devops.controllers;

import lombok.RequiredArgsConstructor;
import ma.mokaddemhicham.devops.services.EnvService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/env")
@RequiredArgsConstructor
public class EnvController {

    private final EnvService envService;

    @GetMapping("")
    public String getEnv() {
        return envService.getEnv();
    }
}
